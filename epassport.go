package main

import (
	"fmt"
	"net/http"
	"net/url"
)

func epassport(username, password string) string {
	resp, err := http.PostForm("http://elogin.rmutsv.ac.th",url.Values{"username": {username}, "password": {password}})
	if err != nil {
		return "error"
	}
	p := make([]byte, resp.ContentLength)
	resp.Body.Read(p)
	return string(p)
}

func main() {
	fmt.Printf("%s", epassport("username","password"))
}
