package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

func epassport2(username, password string) map[string]interface{} {
	resp, err := http.PostForm("https://elogin2.rmutsv.ac.th", url.Values{"username": {username}, "password": {password}})
	if err != nil {
		edat := make(map[string]interface{})
		edat["success"] = "false"
		return edat
	}
	p := make([]byte, resp.ContentLength)
	resp.Body.Read(p)
	rdat := make(map[string]interface{})
	if err := json.Unmarshal(p, &rdat); err != nil {
		edat := make(map[string]interface{})
		edat["success"] = "false"
		return edat
	}
	return rdat
}

func main() {
	fmt.Printf("%s", epassport2("username", "password"))
}
